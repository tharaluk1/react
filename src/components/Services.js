import React, { useState } from 'react'
import Title from './Title'
import {FaCocktail, FaHiking, FaShuttleVan, FaBeer} from 'react-icons/fa'


function Services() {
    const services = useState([
        {
            icon: <FaCocktail />,
            title: 'Free Cocktail',
            info: 'this a lorem content for my one services'
        },
        {
            icon: <FaHiking />,
            title: 'Endless Hiking',
            info: 'this a lorem content for my two services'
        },
        {
            icon: <FaShuttleVan />,
            title: 'Free ShuttleVan',
            info: 'this a lorem content for my three services'
        },
        {
            icon: <FaBeer />,
            title: 'Free Beer',
            info: 'this a lorem content for my four services'
        },
    ]);

    return (
        <section className="services">
            <Title title="Services" />
            <div className="services-center">
                {services[0].map( (item, index) => {
                    return (
                        <article key={index} className="service">
                            <span>{ item.icon }</span>
                            <h6>{ item.title }</h6>
                            <p>{ item.info }</p>
                        </article>
                    )
                })}
            </div>
        </section>
    )
}

export default Services
